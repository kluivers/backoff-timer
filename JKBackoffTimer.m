#import "JKBackoffTimer.h"

@interface JKBackoffTimer 
- (void) retry;
@end

@implementation JKBackoffTimer {
    NSUInteger retryCount;
}

@synthesize slotTime, truncateLimit, retryBlock, retryCount;

- (id) init {
    self = [super init];
    
    if (self) {
        retryCount = 0;
        truncateLimit = DBL_MAX;
    }
    
    return self;
}

+ (id) timerStartedWithSlot:(double)slotTime retryBlock:(void(^)())retryBlock {
    JKBackoffTimer *timer = [[JKBackoffTimer alloc] init];
    timer.slotTime = slotTime;
    timer.retryBlock = retryBlock;
    [timer start];
    
    return timer;
}

- (void) start {
    retryCount = 0;
    [self retry];
}

- (void) success {
    retryCount = 0;
}

- (void) fail {
    retryCount += 1;
    [self retry];
}

- (void) retry {
    
}

@end