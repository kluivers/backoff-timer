#import <Foundation/Foundation.h>

@interface JKBackoffTimer : NSObject

@property(nonatomic, assign) double slotTime;
@property(nonatomic, assign) double truncateLimit;
@property(nonatomic, copy) void(^retryBlock)();
@property(nonatomic, readonly) NSUInteger retryCount;

+ (id) timerStartedWithSlot:(double)slotTime retryBlock:(void(^)())retryBlock;

- (void) start;
- (void) success;
- (void) fail;

@end