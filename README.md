JKBackoffTimer
==============



Requirements
------------
This project is written using ARC. You will not find any memory management in these source files. If you need to use these files in a non-ARC project make sure to specify the `-fno-objc-arc` flag on these files in Build phases > Compile sources.